CS 431/531
Lab 1
Tools, Libraries, Modules, Compilers, Job Submission

--------- Instruction --------- 

1. Module System
  module avail
    - List all modules available on the system
    - Use "module avail [prefix]" to list available modules with the prefix

  module load [module_name] 
    - Load module with name specified name 

  module list
    - List all currently loaded modules

  module purge
    - Remove all currently loaded modules
    - Use "module purge [module_name]" to purge specific modules

2. Compilers
  gcc/g++
   - GNU C/C++ compiler
   - Used for general C/C++ programming

  nvcc
    - Nvidia C/C++ compiler
    - Needed for CUDA programming

  icc
    - Intel C/C++ compiler
    - "mpicc" is the MPI wrapper for the icc compiler

3. Job Submission
  sinfo
    - Report the state of partitions and nodes

  srun [binary]
    - Run a parallel job on cluster managed by SLURM. 

  sbatch [scrpit]
    - Submit a batch script to SLURM. 

  squeue
    - View information about jobs located in the SLURM scheduling queue.
  
  scontrol
    - View and modify SLURM configuration and state.
  
  scancel
    - Signal or cancel jobs or job steps that are under the control of SLURM.

  Each slurm command has an associated help command: <SLURM_COMMAND> --help 

--------- Exercise --------- 
  Part 1 - Remote Access:

    1. Use ssh to log into Cerberus (cerberus.nic.uoregon.edu)

    2. Download mpi-example.c to personal computer.

    3. Use scp to copy mpi-example.c to your home directory on cerberus

  Part 2 - Modules and Compiling:
    1. Load the following modules:
      
        module load intel/17
        module load mpi/intel-17
	module load slurm

    2. Use mpicc to compile example.c (leave executable name as a.out) 


  Part 3 - Running the binary directly 
    1. Say you have a simple MPI application binary, and you want to launch it parallely on a cluster. 
   
    2. We can use the "srun" command to do so. One should tell the command that this is an MPI application. We run it like so:
       srun -N 1 -n 4 --pmi=mpi2 -p nuc ./a.out #4 processes, on the nuc partition using 1 nuc node

       Any output from this command is piped directly to stdout.

  Part 3 - Batch Job:

    0. Say you want to do a whole bunch of setting environment variables, performing some pre-processing in a script, etc before launching the binary.
       For such a situation, "srun" may not be the most efficient way to go. We use a "batch" script to perform all the pre-processing for us before 
       launching the actual parallel application.

    1. Download and scp slurm.sh to your home directory on cerberus

    2. Begin a batch job using the sbatch command (open slurm.sh and look inside to see how one requests for remote "compute" nodes to run your application.) 
       Run: "sbatch slurm.sh"

    3. Review the results in slurm-[job_number].out
    
    4. You now have the skills to launch a real application on a supercomputer! 
