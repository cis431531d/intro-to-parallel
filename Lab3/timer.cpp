// ---------------------------
// includes
#include <iostream>
#include "tbb/task_scheduler_init.h"
#include "tbb/tick_count.h"

// ---------------------------
// before parallel code
  int n = tbb::task_scheduler_init::default_num_threads();
  for (int p = 1; p <= n + 4; ++p) {
    // Construct task scheduler with p threads
    tbb::task_scheduler_init init(p);
    tbb::tick_count t0 = tbb::tick_count::now();


// ---------------------------
// after parallel code
  tbb::tick_count t1 = tbb::tick_count::now();
  double t = (t1 - t0).seconds();
  std::cout << "time = " << t * 1000 << "(ms) with " << p << " threads" << std::endl;
  // Implicitly destroy task scheduler
}
