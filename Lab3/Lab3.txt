CIS 431/531
Lab 3
https://gitlab.com/lamb/Intro-to-Parallel/raw/master/Lab3/Lab3.txt
Task-based parallelism and Intel Thread Building Blocks (TBB)

-------- Instruction --------

1. Intel Thread Building Blocks (TBB)

  C++ Template library enabling parallelism within a program in many different ways. Unlike ispc, TBB is not restricted to for loops and vector processors, although the syntax can be more complicated.


2. TBB parallel_for 

  Serial:
    for (int i = 0; i < N; ++i) {
      // calculate foo
    }

  Parallel:

    class Foo() {
      void operator() (block) {
        // calculate foo over block
      }
    }

    parallel_for( range(0->N), Foo() )



3. TBB tasks 
  Create a task group:
    tbb::task_group g;

  Create new tasks to run in parallel in the task group using:
    g.run(new_task)

  Wait for tasks in a task group to finish running:
    g.wait();


-------- Exercise 1. --------
Use TBB parallel_for to parallelize a loop performing trigonometry on two std::vectors.

  1. Download Lab3/vectrig/vectrig.cpp. Verify that this program compiles and executes using g++.

  2. Include needed header files, "tbb/parallel_for.h" and "tbb/blocked_range.h".

  3. Replace primary computation with parallel_for function call, using tbb::blocked_range() to define the range, and calling a new constructor() as the second argument. 

  4. Define the class VecTrig, setting up the arguments as appropriate, and providing a default constructor. Pass vectors by reference.

  5. Overload the () operator for VecTrig to iterate over a block and perform the main computation.

  6. Compile and test using icc. On cerberus, you many need to load the following modules:
      intel/17
      intel/tbb

      icc -o vectrig_tbb vectrig_tbb.cpp -ltbb

  7. Add code from Lab3/timer.cpp, and recompile/execute. Notice how the runtime changes w
ith the number of threads requested. 

-------- Exercise 2. --------
Use TBB tasking to parallelize a recursive Fibonacci function.

  1. Download, compile and test the following program:
      Lab3/fib/fib.cpp 
  
  2. Include the needed header: "tbb/task_group.h"

  2. Create a task group for each recursion of Fib()

  3. Create "tasks" (lambda functions/ functors) from the child calculations, and assign these tasks to the task group created in (2) using the task group run() member function. 

    Example:
      x = x + 1; -> [&] {x = x + 1};

  4. Wait for both child tasks to finish before returning to the previous step of the recursion.

  5. Compile and test code using icc:

    icc -std=c++11 -o fib_tbb fib_tbb.cpp -ltbb 

  6. Add code from Lab3/timer.cpp, and recompile/execute. Notice how the runtime changes with the number of threads requested.
